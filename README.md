Write a simple 1D DFT code in Python

This is a tutorial.  We will step by step write our own density
functional theory (DFT) code for one-dimensional systems in Python.

Solution scripts and explanatory text included.

The exercise was originally written for the Nanoscience Summer School
2017 at Yachay Tech University, Urcuquí, Ecuador.
